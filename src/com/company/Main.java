package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("Введите текст с большой буквы! ");
        Scanner countSentence = new Scanner(System.in);
        String s =  countSentence.nextLine();
        int numbers = 0;
        for(int i = 0; i < s.length() - 1; i++) {
            if ((s.charAt(i+1) == '.' || s.charAt(i+1) == '!' ||
                    s.charAt(i+1) == '?') &&
                    !(s.charAt(i) == '.'   || s.charAt(i) == '!'   ||
                            s.charAt(i) == '?')) numbers++;
        }
        System.out.println(numbers);
    }
}
